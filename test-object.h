#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define TEST_TYPE_OBJECT (test_object_get_type())

G_DECLARE_DERIVABLE_TYPE (TestObject, test_object, TEST, OBJECT, GObject)

struct _TestObjectClass
{
  GObjectClass parent_class;
};

TestObject *test_object_new (gint prop);
void test_object_emit_foo (TestObject *self);

G_END_DECLS

#define G_LOG_DOMAIN "test-object.h"

#include "test-object.h"

typedef struct
{
  gint prop;
} TestObjectPrivate;

enum {
  PROP_0,
  PROP_PROP,
  N_PROPS
};

enum {
  FOO,
  N_SIGNALS
};

G_DEFINE_TYPE_WITH_PRIVATE (TestObject, test_object, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
test_object_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  TestObject *self = TEST_OBJECT (object);
  TestObjectPrivate *priv = test_object_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PROP:
      g_value_set_int (value, priv->prop);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
test_object_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  TestObject *self = TEST_OBJECT (object);
  TestObjectPrivate *priv = test_object_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PROP:
      priv->prop = g_value_get_int (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
test_object_class_init (TestObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = test_object_get_property;
  object_class->set_property = test_object_set_property;

  properties [PROP_PROP] =
    g_param_spec_int ("prop", NULL, NULL,
                      0, 10, 0,
                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  
  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [FOO] =
    g_signal_new ("foo",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
test_object_init (TestObject *self)
{
}

TestObject *
test_object_new (gint prop)
{
  return g_object_new (TEST_TYPE_OBJECT, "prop", prop, NULL);
}

void
test_object_emit_foo (TestObject *self)
{
  g_signal_emit (self, signals [FOO], 0);
}

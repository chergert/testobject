all: test-object

test-object: test-object.c test-object.h test.c Makefile
	$(CC) -o $@ -Wall test-object.c test.c $(shell pkg-config --cflags --libs gobject-2.0)

clean:
	rm -f test-object
